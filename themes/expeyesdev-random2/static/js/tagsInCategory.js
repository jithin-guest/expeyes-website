function hideEmptyTags(){
  var tags=document.querySelectorAll(".tag");
  // hide tags with no article in this category
  for (var i=0; i < tags.length; i++){
    var list=tags[i].querySelectorAll("li");
    if (list.length==0){
      tags[i].classList.remove("visible");
      tags[i].classList.add("hidden");
    }	    
  }
}

/**
 * reorders the tags in the page, and strips their leading digits if any
 **/
function reorderTags(){
    var pageBody=document.querySelector("div#page-body");
    var tags=pageBody.querySelectorAll(".tag");
    // remove pageBody's children
    var lesTags=new Object();
    var cles=[];
    for (var i=0; i<tags.length; i++) {
	var tag=tags[i];
	var cle=tag.querySelector("h2").innerHTML;
	// remove leading digits
	tag.querySelector("h2").innerHTML=cle.replace(/^[0-9]+/,"");
	pageBody.removeChild(tag);
	lesTags[cle]=tag;
	cles.push(cle);
    }
    // insert sorted tags
    cles.sort();
    for (var i=0; i<cles.length; i++) {
	pageBody.appendChild(lesTags[cles[i]]);
    }
}

/**
 * Translation of the tags harvested from the source and
 * rendered as subtitles
 **/
function translateTags(){
    document.querySelector("#known-categories").setAttribute("style","display:none");
    var terms=document.querySelectorAll("#known-categories dt");
    var trans=document.querySelectorAll("#known-categories dd");
    var dico=new Object();
    for (var i=0; i < terms.length; i++){
	dico[terms[i].innerHTML]=trans[i].innerHTML;
    }
    var tr=document.querySelectorAll(".translatable");
    for (var i=0; i < tr.length; i++){
	var h=tr[i];
	if (h.innerHTML in dico) h.innerHTML=dico[h.innerHTML];
    }
}
