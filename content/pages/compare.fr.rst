Comparaison entre ExpEYES-17 et ExpEYES Junior
##############################################

:slug: compare
:lang: fr
       
Le principal inconvénient d'ExpEYES Junior est l'absence de fréquence variable
pour le signal sinusoïdal et la limitation des calibres d'entrée
à +/-5V. La nouvelle version y remédie, tout en ajoutant de nombreuses
améliorations pour la vitesse d'échantillonnage, la taille du tampon en
mémoire,  etc. La complexité accrue du matériel augment aussi le coût
unitaire du boîtier, mais les deux versions sont disponibles.

+---------------------------+------------------------------+-------------------------------------------+
| Propriété                 | ExpEYES Junior               | ExpEYES-17                                |
+---------------------------+------------------------------+-------------------------------------------+
| Vitesse d'échantillonnage | 250 kéch/s, 4 canaux         | 3 Méch/s, 4 canaux                        |
+---------------------------+------------------------------+-------------------------------------------+
| Calibre des entrées       | fixe, ± 5V                   | programmable, de ± 500 mV à ± 16V         |
+---------------------------+------------------------------+-------------------------------------------+
| Générateur BF             | fixe : 150 Hz, 3V sinusoïdal | Sinus/Triangle : 5Hz à 5kHz, 3V, 1V, 80mV |
+---------------------------+------------------------------+-------------------------------------------+
| paramètres du µCrs        | PIC 8MHz, 2ko RAM            | PIC 64MHz, 32ko RAM                       |
+---------------------------+------------------------------+-------------------------------------------+
| Sorties analogiques       | 0 à 5V seulement             | -5V à +5V et - 3,3V à +3,3V               |
+---------------------------+------------------------------+-------------------------------------------+
| Connecteurs               | Bornes à vissages            | Connecteurs à ressorts                    |
+---------------------------+------------------------------+-------------------------------------------+
| Support de modules I2C    | Non                          | Oui                                       |
+---------------------------+------------------------------+-------------------------------------------+
| Sortie de tension d'alim. | Non                          | +5V/200mA et ± 6V, 5mA                    |
+---------------------------+------------------------------+-------------------------------------------+

+-----------------------------------+--------------------------------+
| .. image:: /images/exp-junior.jpg | .. image:: /images/eyes-17.jpg |
|     :alt: Expeyes-Junior          |     :alt: Eyes-17              |
|                                   |     :width: 633px              |
+-----------------------------------+--------------------------------+
