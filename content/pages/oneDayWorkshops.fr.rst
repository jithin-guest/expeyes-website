Ateliers d'un jour
##################

:slug: oneDayWorkshops
:lang: fr

L'objectif des « Ateliers d'un jour » c'est d'introduire les expériences
interfacées à un ordinateur, à l'aide d'ExpEYES. Les expériences sont
maontrées et le détail des implémentations sont expliqués durant ces
programmes. Le prérequis est une salle de conférence avec un
vidéoprojecteur.

L'institution hôte qui souhaite organiser un tel programme
doit faire en sorte de faciliter la participation des professeurs et
étudiants des institutions voisines.
