Comparison between ExpEYES-17 and ExpEYES Junior
################################################

:slug: compare
:lang: en
       
The main drawback of EJ is the lack of variable frequency sine wave
and the limited analog input range of +/-5V. The new version remedies
along with other enhancements in sampling rate, buffer size etc. The
increased hardware complexity also increases the cost of the unit, but
both the versions are available.

+----------------------+----------------------------+-------------------------------------------+
| Feature              | ExpEYES Junior             | ExpEYES-17                                |
+----------------------+----------------------------+-------------------------------------------+
| Analog Sampling Rate | 250 ksps / 4chan           | 3 Msps/ 4chan                             |
+----------------------+----------------------------+-------------------------------------------+
| Analog Input Range   | ± 5V fixed                 | ± 500 mV to ± 16V programmable            |
+----------------------+----------------------------+-------------------------------------------+
| Waveform Generator   | fixed 150 Hz /3V sine wave | Sine/Triangular 5Hz to 5kHz, 3V, 1V, 80mV |
+----------------------+----------------------------+-------------------------------------------+
| µC parameters        | PIC 8MHz, 2kB RAM          | PIC 64MHz, 32kB RAM                       |
+----------------------+----------------------------+-------------------------------------------+
| Analog Outputs       | 0 to 5V only               | -5V to +5V and - 3.3V to +3.3V            |
+----------------------+----------------------------+-------------------------------------------+
| Connectors           | Screw terminals            | Spring loaded push connectors             |
+----------------------+----------------------------+-------------------------------------------+
| I2C module support   | No                         | Yes                                       |
+----------------------+----------------------------+-------------------------------------------+
| Power Supply outputs | No                         | +5V/200mA and ± 6V, 5mA                   |
+----------------------+----------------------------+-------------------------------------------+

+-----------------------------------+--------------------------------+
| .. image:: /images/exp-junior.jpg | .. image:: /images/eyes-17.jpg |
|     :alt: Expeyes-Junior          |     :alt: Eyes-17              |
|                                   |     :width: 633px              |
+-----------------------------------+--------------------------------+
