:slug: Expt17/html/measure-dc
:date: 2108-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Mesure de tension continue
:lang: fr
       
Mesure de tension continue
##########################

|image0|

-  Connecter la pile entre masse (borne noire) et A1 comme montré sur
   la figure, mesurer la tension en A1.
-  Recommencer en inversant les bornes. La tension mesurée l'est en
   référence à la borne de masse.

.. |image0| image:: schematics/measure-dc.svg
		    :width: 500px
