:slug: Expt17/html/measure-dc
:date: 2108-09-07
:category: schoollevel
:tags: 1School Level Activities
:title: Measuring DC voltage
:lang: en
       
Measuring DC voltage
####################

|image0|

-  Connect the cell between Ground and A1 as shown in the figure,
   measure the voltage at A1.
-  Repeat by reversing the terminals. The measured voltage is with
   respect to the Ground terminals

.. |image0| image:: schematics/measure-dc.svg
		    :width: 500px
