:slug: Expt17/html/soundBeats
:date: 2108-09-07
:category: expts_17
:tags: 3Sound (velocity and beats)
:title: Battements sonores
:lang: fr
       
Battements sonores
##################

Deux buzzers piézo-électriques sont pilotés par WG et SQ1 respectivement.
Les fréquences sont réglées proche l'une de l'autre.


|image0|

|image1|

.. |image0| image:: schematics/sound-beats.svg
   :width: 500px
.. |image1| image:: photos/sound-beats.jpg

