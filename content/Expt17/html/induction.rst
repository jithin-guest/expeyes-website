:slug: Expt17/html/induction
:date: 2108-09-07
:category: expts_17
:tags: 2Electrical Circuits and LCR elements
:title: Electromagnetic Induction
:lang: en

Electromagnetic Induction
#########################

A magnet is dropped into a coil and the resulting voltage signal is
plotted as a function of time. The experimental setup is shown below,
together with captured waveform.

+----------+----------+
| |image0| | |image1| |
+----------+----------+


.. |image0| image:: schematics/induction.svg
.. |image1| image:: photos/em-induction.jpg

