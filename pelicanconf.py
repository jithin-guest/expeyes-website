#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Ajith Kumar'
SITENAME = u'Expeyes'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
FEED_RSS="feeds/rss.xml"


# Blogroll
LINKS = (
    ("Framasoft","https://framasoft.org/"),
    ("La Quadrature du Net","https://www.laquadrature.net/fr/"),
)

# Social widget
SOCIAL = (('Mastodon', 'https://mastodon.social/about'),
          ('Diaspora-Fr', 'https://diaspora-fr.org/'),)

DEFAULT_PAGINATION = 10

THEME = 'themes/expeyesdev-random2'

PAGE_PATHS = ['pages','images/favicon.png']
STATIC_PATHS = ['images','icons','Documents','Expt17','Code', 'MicroHOPE']
EXTRA_PATH_METADATA = {
    'images/favicon.png': {'path': 'favicon.png'}
}

SLUGIFY_SOURCE = 'title'

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

#### PLUGINS
PLUGIN_PATHS = ['plugins']
PLUGINS = ['i18n_subsites',]

#### I18N
main_lang = u"en"
main_site_url = u""

I18N_SUBSITES = {
    'fr': {
        'SITENAME': u'ExpEYES',
        'THEME': u'themes/expeyesdev-random2',
        'STATIC_PATHS': [u'images',u'icons',u'Documents',u'Expt17',u'Code', u'MicroHOPE'],
    },
}

JINJA_ENVIRONMENT = {
    'extensions': [u'jinja2.ext.i18n'],
}

I18N_GETTEXT_LOCALEDIR = THEME+u'/lang'
I18N_GETTEXT_DOMAIN = u'expeyes'
